use ModernWays;
insert into boeken (
Id,
Voornaam,
Familienaam,
Titel,
Stad,
Uitgeverij,
Verschijningsdatum,
Herdruk,
Commentaar,
Categorie
)
values
('Samuel', 'Ijsseling', 'Heidegger. Denken en Zijn. Geven en Danken', 'Amsterdagm', '', '2014', '', 'Nog te lezen', 'Filosofie',),
('Jacob', 'Van Sluis', 'Lees wijzer bij Zijn en Tijd', '', 'Budel', '1998', '', 'Goed boek', 'Filosofie',);